# DEVELOP CHECKMK EXTENSIONS

## About

There is a CI-Pipeline in GitLab, that creates a new Docker container when a new branch is being created. This container contains a fresh CheckMK installation. You can use the Remote Containers extension in VS Code to connect to the container and access all the CheckMK files. You can clone your repository local and mount it in the container or clone it directly into the container. When the branch gets merged the container will be deleted by the pipeline.

## Prerequisites

- Docker - check it with:

```bash
docker --version
```

- Visual Studio Code
- Visual Studio Code extension ```Remote Containers``` or ```Dev Containers```

## Workflow Explanation

1. [ ] Create a new branch
2. [ ] Use the Remote Containers Extension in VSCode to connect to the created container
3. [ ] Develop extensions
4. [ ] Test extensions (find admin password in the containers logfile)
5. [ ] Create new folder for each extension in the repository and write a short readme
6. [ ] Move your files to the folder and push your changes
7. [ ] Merge your branch to main
