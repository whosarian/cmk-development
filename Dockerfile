FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive
RUN chmod +x /usr/sbin/policy-rc.d

ARG CMK_VERSION="2.2.0p27"
ARG CMK_EDITION="raw"

# The following variables can be set during container init (docker run -e KEY=val)
ARG CMK_SITE_ID
ENV CMK_SITE_ID="dev"
# Install required packages
RUN apt update && apt install -y \
    wget \
    libcap2-bin \
    cron \
    time \
    traceroute \
    curl \
    dialog \
    graphviz \
    apache2 \
    apache2-utils \
    libevent-2.1-7 \
    libltdl7 \
    libnl-3-200 \
    libpangocairo-1.0-0 \
    libperl5.34 \
    libreadline8 \
    libxml2 \
    logrotate \
    php-cli \
    php-cgi \
    php-gd \
    php-sqlite3 \
    php-json \
    php-pear \
    rsync \
    smbclient \
    rpcbind \
    unzip \
    xinetd \
    freeradius-utils \
    libpcap0.8-dev \
    libcap-dev \
    dnsutils \
    rpm \
    binutils \
    libgsf-1-114 \
    libglib2.0-0 \
    cpio \
    libfl2 \
    poppler-utils \
    lcab \
    libpq5 \
    git

RUN apt-get clean
RUN rm /usr/sbin/policy-rc.d
# Create a directory for the Checkmk installation
RUN mkdir -p /opt/checkmk

ENV SITE_ID=my_site_id
# Download and install Checkmk Raw Edition
RUN wget -O /opt/checkmk/check_mk.deb https://download.checkmk.com/checkmk/2.2.0p27/check-mk-raw-2.2.0p27_0.jammy_amd64.deb && \
    dpkg -i /opt/checkmk/check_mk.deb && \
    rm /opt/checkmk/check_mk.deb

# Copy entrypoint script
COPY docker-entrypoint.sh /docker-entrypoint.sh

# Make entrypoint script executable
RUN chmod +x /docker-entrypoint.sh

LABEL \
    org.opencontainers.image.title="Checkmk" \
    org.opencontainers.image.version="2.2.0p27" \
    org.opencontainers.image.description="Checkmk is a leading tool for Infrastructure & Application Monitoring" \
    org.opencontainers.image.vendor="Checkmk GmbH" \
    org.opencontainers.image.source="https://github.com/checkmk/checkmk" \
    org.opencontainers.image.url="https://checkmk.com/"

# Expose required ports
EXPOSE 5000/tcp

# Define entrypoint
ENTRYPOINT ["/docker-entrypoint.sh"]